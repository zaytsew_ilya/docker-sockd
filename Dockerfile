FROM ubuntu

ARG version
ARG user
ARG password

RUN apt-get update && apt-get install -y \
      wget \
      libwrap0-dev \
      build-essential \
    && rm -rf /var/lib/apt/lists/*

RUN wget https://www.inet.no/dante/files/dante-$version.tar.gz && \
    tar xzf dante-$version.tar.gz && \
    rm dante-$version.tar.gz && \
    cd dante-$version && \
    ./configure && make && make install

COPY sockd.conf /etc/sockd.conf

RUN useradd qwe && echo $user:$password | chpasswd

CMD sockd
